
(function(d){
	const addTaskNode = d.querySelector("#addTask")
	const writeTaskNode = d.querySelector("#writeTask")
	const displayTaskNode = d.querySelector("#displayTask")
	const addTask = d.querySelector("#addTask")

	writeTaskNode.onkeyup = function(e) {
		if(e.keyCode === 13){
			addNewTask(displayTaskNode, e.target.value)
			writeTaskNode.value = ''
		}
	}
	addTask.onclick = function(e) {	
		if(writeTaskNode.value.trim()){
			addNewTask(displayTaskNode, writeTaskNode.value)
			writeTaskNode.value = ''			
		}else{
			alert("Please put some data")
		}
	}

})(document)


function addNewTask(displayTaskNode, task){
	let colNode = document.createElement('div')
	colNode.className = "col-sm-3 col-xs-12"

	let alertNode = document.createElement("div")
	alertNode.className = "alert"
	alertNode.style.backgroundColor = "cyan"
	let closeNode = document.createElement("a")
	closeNode.innerText = "×"
	closeNode.className = "close"

	alertNode.appendChild(closeNode)
	let textNode = document.createElement("span")
	textNode.innerText =  `${task.toUpperCase()}`
	alertNode.appendChild(textNode)


	colNode.appendChild(alertNode)
	displayTaskNode.appendChild(colNode)

	closeNode.onclick = function(e){
		displayTaskNode.removeChild(this.parentNode.parentNode)
	}

	colNode.onmouseenter = function(e) {
		showColorPallet(this)
	}

	colNode.onmouseleave = function(e) {
		hideColorPallet(this)
	}
}

function showColorPallet(colNode){
	const colors = ['red','lightblue','green','pink']
	let colorGroup = document.createElement('ul')
	colorGroup.className = "color_group"

	colors.forEach(color => {
		let colorListItem = document.createElement('li')
		colorListItem.className = "color_item"
		colorListItem.style.backgroundColor = `${color}`
		colorGroup.appendChild(colorListItem)
		colorListItem.onclick = function(e){
			colorListItem.parentNode.parentNode.style.backgroundColor = `${this.style.backgroundColor}`
		}
	})

	/* creating edit button */
	createEditBtn(colorGroup)

	/* apending color group list to alert panel section */
	colNode.querySelector('.alert').appendChild(colorGroup)

}
function hideColorPallet(colNode){	
	let colorGroup = document.querySelector('.color_group')	
	colorGroup.parentNode.removeChild(colorGroup)
}

function createEditBtn(colorGroup){
	let editBtn = document.createElement("li")
	editBtn.className = "color_item"
	editBtn.setAttribute("title","Click to Edit")
	let editIcon = document.createElement("i")
	editIcon.className = "glyphicon glyphicon-pencil"
	editBtn.appendChild(editIcon)
	colorGroup.appendChild(editBtn)	


	/* event on edit button */
	editBtn.onclick = function(e){
		parentNode = this.parentNode.parentNode
		createTaskEditArea(parentNode)		
		
	}
}

function createTaskEditArea(parentNode){
	taskDetailsNode = parentNode.querySelector("span").innerText.toLowerCase()
	let taskTextarea = document.createElement("textarea")
	taskTextarea.className = "edit_task_area"
	taskTextarea.style.width = parentNode.offsetWidth + "px"
	taskTextarea.style.height = parentNode.offsetHeight + "px"
	taskTextarea.innerText = taskDetailsNode
	parentNode.appendChild(taskTextarea)
	taskTextarea.onkeyup = function(e){
		e.stopPropagation()
		if(e.keyCode === 13){
			
			if( this.value.trim() ){
				parentNode.querySelector("span").innerText = this.value.toUpperCase()
				parentNode.removeChild(this)
			}else{
				alert("Please put some data")
			}
		}
	}
}